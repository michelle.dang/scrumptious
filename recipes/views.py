from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id = id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


def create_recipe(request):
    if request.method == "POST":
        # Use the form to validate the values & save them to database
        form = RecipeForm(request.POST)
        # If all goes well, can redirect browser to
        # another page & leave the function
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        # create an instance of Django model form class
        form = RecipeForm()

        # Put the form in the context
    context = {
            "form": form,
    }
        # Render the HTML template w the form
    return render(request, "recipes/create.html", context)


def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id = id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "recipe_object": recipe,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)
